#University ID: 201262440, Filename: Marinov_Matey-CA07.py
#December 2017
#Creating a 5x5 game field with players to play and to try to create a 2x2 square of same number to win

import random

def no_spaces_left(game_matrix):
    global game_end
    
    #Checking if there are any free positions left
    if any(0 in sublist for sublist in game_matrix) == True:
        return True
    else:
        game_end = False
        return False


def win_condition(row, column, game_matrix):
    global game_end

    #Checking if there exists a square 2x2 with the same number and prints 
    if game_matrix[row][column] == 1 and game_matrix[row][column+1] == 1 and game_matrix[row+1][column] == 1 and game_matrix[row+1][column+1] == 1:
        matrix_print(game_matrix)
        print("Player 1 WON the game!")
        print("Thank you for playing!")
        game_end = True
        
    elif game_matrix[row][column] == 2 and game_matrix[row][column+1] == 2 and game_matrix[row+1][column] == 2 and game_matrix[row+1][column+1] == 2:
        matrix_print(game_matrix)
        
        if computer_or_player == "player":
            print("Player 2 Won the game!")
            print("Thank you for playing!")
            game_end = True
            
        else:
            print("The computer Won the game!")
            print("Thank you for playing!")
            game_end = True


def board_validation(game_matrix):
    global player_turn
    global game_end
    global computer_or_player

    #Checking if there exists a 2x2 square with the same number
    for row in range(4):
        for column in range(4):
            if game_matrix[row][column] != 0:
                win_condition(row, column, game_matrix)
        

def position_taken(game_matrix, input_number):
    global player_turn
    global computer_or_player
    input_number = input_number - 1

    #Checking if the position is already taken
    if game_matrix[input_number//5][input_number % 5] == 1 or game_matrix[input_number//5][input_number % 5] == 2:
        if player_turn % 2 != 0:
            print("This position is already taken. Try again")

        elif player_turn % 2 == 0 and computer_or_player == "player":
            print("This position is already taken. Try again")
        #Run position input if the selected position by the computer is already
        else:
            position_input(game_matrix)

    #Putting either 1 or 2 in the game field depending on whos turn it is
    elif player_turn % 2 != 0:
        game_matrix[input_number//5][input_number % 5] = 1
        player_turn = player_turn + 1

    elif player_turn % 2 == 0:
        if computer_or_player == "computer":
            print("The computer selected number", input_number+1)
        game_matrix[input_number//5][input_number % 5] = 2
        player_turn = player_turn + 1

    return game_matrix


def player_turns():
    global player_turn
    global computer_or_player

    #Trying if the value is correct type
    try:
        #Prompt player to input the position on which to place number
        if player_turn % 2 != 0:
            input_number = int(input("Player 1, Enter the place you want to put your number: "))
        elif player_turn % 2 == 0 and computer_or_player == "player":
            input_number = int(input("Player 2, Enter the place you want to put your number: "))
        else:
            #Genereting random number for computer's turn
            input_number = random.randint(1, 25)
        return input_number
    
    #If value is incorrect type run the function again
    except ValueError:
        print("Enter a valid number!")
        input_number = player_turns()
        return input_number
    

def position_input(game_matrix):
    global player_turn
    
    input_number = player_turns()
        
    #Checking if number is out of range
    if input_number < 1 or input_number > 25:
        print("The number you entered is not in the game range")

    else:
        game_matrix = position_taken(game_matrix, input_number)

    return(game_matrix)


def matrix_print(game_matrix):

    #Printing game board
    for row in game_matrix:
        for item in row:
            print(item, end="   ")
        print()
        print()
    print()

def main_game(game_matrix):

    global play_again
    global game_end
    
    while game_end == False:

        #Priting the game matrix
        matrix_print(game_matrix)

        game_matrix = position_input(game_matrix)

        #Checking if any player has won the game
        board_validation(game_matrix)

       #Checking if there are no spaces left
        if no_spaces_left(game_matrix) == True:
            continue
        else:
            #Checking if game is a draw
            matrix_print(game_matrix)
            print("There are no more free positions. The game is a draw!")
            print("Thank you for playing!")
            game_end = True
            continue
    else:
        play_again = str(input("Would you play again ?(yes, no): "))
        play_again = play_again.lower()
        if play_again == "yes":
            print()
            game_start()
        else:
            print("Bye :)")
            main()


def game_start():

    #Initialising variables
    global computer_or_player
    global game_end
    global player_turn

    game_matrix = [[0 for row in range(5)] for col in range(5)]

    game_end = False
    player_turn = 1

    computer_or_player = str(input("Who would you like to play against (computer, player) ?: "))
    computer_or_player = computer_or_player.lower()

    #Validating if it is written correctly
    while computer_or_player not in ("computer", "player"):
        print("Please enter a valid option: computer or player")
        print()
        game_start()

    #Executing main_game fucntion
    main_game(game_matrix)


#Defining the main menu function
def main():
    while True:
        print('''\n\tMain Menu
\tA. Numbers
\tB. Strings
\tC. Games
\tX. Exit\n''')

        option = input("Enter an option (A, B, C or X): ")
        option = option.upper() #making the option upper case in case the user used lower
        #Checking for each option and executing the defined function
        if option == "A":
            print("Nothing here yet")

        elif option == "B":
            print("Nothing here yet")

        elif option == "C":
            game_start()

        elif option == "X":
            break
        else:
            print("Please enter a valid option!")

#Starting main menu loop function
main()
