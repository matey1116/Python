#University ID: 201262440, Marinov_matey-CA04.py
#November 2017
#Determine the outcome of 4 dice
import random

#Defining game function
def validation(numbers):
    
    #Creating a set from the list to remove any dublicate numbers for further testing
    abc = set(numbers)

    print()

    #For the validation of the dice I am using set to remove duplciates and compare the length of the set with the list and list.count to count the same numbers
    
    #Checking if all dice numbers are the same
    if numbers.count(numbers[0]) == 4:
        print("All 4 dice are with the same number")
        return
    
    #Checking if 3 of the dice numbers are the same
    elif numbers.count(numbers[0]) == 3 or numbers.count(numbers[1]) == 3:
        print("3 of the dice are the same")
        return
    
    #Checking if there is a run of 4 numbers
    elif numbers[0] == numbers[1] - 1 and numbers[1] == numbers[2] - 1 and numbers[2] == numbers[3] - 1:
        print("This is a run of four")
        return
    
    #Checking if there is a run of 3 numbers and a pair
    elif numbers[0] == numbers[1] - 1 and numbers[1] == numbers[2] - 1 and numbers[2] != numbers[3] - 1 and len(abc) == 3:
        print("This is a run of three with a pair")
        return

    elif numbers[0] != numbers[1] - 1 and numbers[1] == numbers[2] - 1 and numbers[2] == numbers[3] - 1 and len(abc) == 3:
        print("This is a run of three with a pair")
        return
    
    elif numbers[0] == numbers[1] - 1 and numbers[1] == numbers[2] and numbers[2] == numbers[3] - 1 and len(abc) == 3:
        print("This is a run of three with a pair")
        return

    #Checking if there is a run of 3 numbers only
    elif numbers[0] == numbers[1] - 1 and numbers[1] == numbers[2] - 1 and numbers[2] != numbers[3] - 1:
        print("This is a run of three")
        return
    
    elif numbers[0] != numbers[1] - 1 and numbers[1] == numbers[2] - 1 and numbers[2] == numbers[3] - 1:
        print("This is a run of three")
        return

    #Checking if there is a double pair
    elif numbers.count(numbers[0]) ==  2 and len(abc) == 2:
        print("This is a double pair")
        return

    #Checking if there is a single pair only
    elif len(numbers) == len(abc) + 1:
        print("This is a single pair")
        return
    
    else:
        print("All 4 dice have different numbers")
        return
    
#Defining game function
def game():
    
    #Declaring the set and variables
    numbers = []

    #Generating and adding the 4 dice numbers to the set
    numbers.append(random.randint(1,6))
    numbers.append(random.randint(1,6))
    numbers.append(random.randint(1,6))
    numbers.append(random.randint(1,6))

    #sorting the set from smallest to largest number
    
    print()
    print("After the roll of three dice:")
    print("Score for die 1 is", numbers[0])
    print("Score for die 2 is", numbers[1])
    print("Score for die 3 is", numbers[2])
    print("Score for die 4 is", numbers[3])

    #sorting the set from smallest to largest number
    numbers = sorted(numbers)
    
    print("Three dice have scores: " + str(numbers[0]) + ", " + str(numbers[1]) + ", " + str(numbers[2]) + ", " + str(numbers[3]))
    
    #Starting validation function
    validation(numbers)


#Defining the main menu function
def main():
    while True:
        print('''\n\tMain Menu
\tA. Numbers
\tB. Strings
\tC. Games
\tX. Exit\n''')
        
        option = input("Enter an option (A, B, C or X): ")
        option = option.upper() #making the option upper case in case the user used lower
        #Checking for each option and executing the defined function
        if option == "A":
            print("Nothing here yet")
			
        elif option == "B":
            print("Nothing here yet")
			
        elif option == "C":
            game();

        elif option == "X":
            break
        else:
            print("Please enter a valid option!")
            
#Starting main menu loop function
main()


