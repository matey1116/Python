# University ID: 201262440, Marinov_Matey-CA01.py
# October 2017
# Develop a program given length, breadth and height to calculate the draft

# Input
# Values may not always be integers so float is better
length = float(input("Enter the length of the barge in meters: "))
breadth = float(input("Enter the breadth of the barge in meters: "))
height = float(input("Enter the height of the barge in meters: "))

# Calculations
iron_weight = 1.06

surface_area = (2 * height) * (length + breadth) + (length * breadth)

barge_mass = surface_area * iron_weight

draft = barge_mass / (length*breadth)

# Printing inputs and calculated draft
print("A barge with length: {:1.2f}m, breadth: {:1.2f}m and height {:1.2f}m"
      " will have draft: {:1.2f}m".format(length, breadth, height, draft))

# L B H = draft
# 2 3 4 = 8.13
# 1 2 3 = 10.6
