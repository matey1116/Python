#University ID: 201262440, filename: Marinov_Matey-CAo5.py
#November 2017
#Hangman game

import random

#Starting the game function
def game(chosen_word):
    
    print("The length of the word is:" , len(chosen_word))

    #Initialising variables
    guesses=0
    correct_letters = []
    wrong_letters = []
    matched_letters = ""
    word_display = []
    correct_string = ""
    wrong_string = ""
    
    #Creating the graphical representation with underscore
    for x in range(len(chosen_word)):
        word_display.append('_')

    #Checking if the user has used all his attempts
    while guesses < len(chosen_word):

        #Showing how many attpents the user has made
        if guesses != 0:
            print("You have made",guesses,"attempts")

        #Input letter by the user
        guess=input("Please enter the letter you guess: ")
        
        guess = guess.lower()

        #Checking if it is the first guess
        if guesses != 0:
            #Checking if the guessed letter is already trued
            if guess in correct_letters:
                print("You have already tried this letter. It will not be added again and this guess will not be counted")
                continue
            
            elif guess in wrong_letters:
                print("You have already tried this letter. It will not be added again and this guess will not be counted")
                continue
            
            #Adding 1 to the number of guesses
            else:
                guesses = guesses + 1
        #Adding 1 to the number of guesses
        else:
            guesses = guesses + 1
        
        #Checking if the guessed letter is in the word
        if guess in chosen_word:
    
            print("The letter is in the word.")
            print()

            #Finding the position of the guessed letter in the word
            for index, letter in enumerate(chosen_word):
                if guess == letter:
                    word_display[index] = guess
            #adding the guessed letter to the correct letters
            correct_letters.append(guess)
            
        else:
                
            print("The letter is not in the word.")
            print()
            
            #Adding the guessed letter to the wrong letters
            wrong_letters.append(guess)
        
        #Creating a string of the correct/wrong letters to output to user    
        correct_string  = ', '.join(correct_letters)
        wrong_string    = ', '.join(wrong_letters)

        #Adding the empty underscores to the matched letters to display to user
        matched_letters = ' '.join(word_display)
    
        print("You have guessed correctly these letters:",correct_string)
        print("You have guessed incorrectly these letters:",wrong_string)
        print("Letters matched so far:", matched_letters)
        print()

        #Removing spaces from the matched letters string
        modified_matched_letters = matched_letters.replace(" ", "")


        #Checking if all of the letters of the word have been guessed by the user
        if modified_matched_letters == chosen_word:

            #Prompting to input the complete word
            last_try = input("Would you like to guess the word: ")
            print()

            #Checking if the inputed word is correct
            if last_try == chosen_word:
                print("You have guessed correctly these letters:",correct_string)
                print("You have guessed incorrectly these letters:",wrong_string)
                
                print("You have guessed the word. It is:",chosen_word)
                print("You WON!!!")
                
            else:
                print("You did not manage to guess the word.")
                print("The correct word was:",chosen_word)
            break

        #Checking if the user has ran out of attempts 
        elif guesses == len(chosen_word):

            #Printing the correct and incorrect letters
            print("You ran out of attempts.")
            print("You have guessed correctly these letters:",correct_string)
            print("You have guessed incorrectly these letters:",wrong_string)

            last_try = input("Try to guess the word based on the guessed letters: ")
            print()

            #If the inputed word is correct
            if last_try == chosen_word:
                print("You have guessed the word. It is:", chosen_word)
                print("You WON!!!")
                
            else:
                print("You did not manage to guess the word.")
                print("The correct word was:",chosen_word)

#Defining the different animal classes for the user to select and starting the game
def classes():

    #Initialising animal classes
    mammals    = ["human","bat","cat","dog","deer","bear","wolf","lion","squirrel","horse","primate","pig","tiger","skunk","sheep","elk","lynx"]
    birds      = ["sparrow","pigeons","dove","parrot","woodpecker","finch","owl","gull","crow","goose","chicken","swan","raven","eagle","crane"]
    fish       = ["tuna","salmon","catfish","cod","pangasius"]
    reptiles   = ["turtle","lizard","snakes","crocodile","alligator","chameleons","iguana","python"]
    amphibians = ["frog","salamander"]

    print("Welcome to Hangman!")
    print()
    
    
    print("Available animal classes: Mammals, Birds, Fish, Reptiles and Amphibians")
    animal_type = input("Select the animal class of the animal you are going to try to guess: ")
    animal_type = animal_type.lower()

    
    if animal_type == "mammals":
        #randomly select animal from the selected class
        chosen_word = random.choice(mammals)
        game(chosen_word)
        
    elif animal_type == "birds":
        #randomly select animal from the selected class
        chosen_word = random.choice(birds)
        game(chosen_word)
        
    elif animal_type == "fish":
        #randomly select animal from the selected class
        chosen_word = random.choice(fish)
        game(chosen_word)

    elif animal_type == "reptiles":
        #randomly select animal from the selected class
        chosen_word = random.choice(reptiles)
        game(chosen_word)
        
    elif animal_type == "amphibians":
        #randomly select animal from the selected class
        chosen_word = random.choice(amphibians)
        game(chosen_word)

    else:
        print("You did not select a valid animal class")
    
    
#Defining the main menu function
def main():
    while True:
        print('''\n\tMain Menu
\tA. Numbers
\tB. Strings
\tC. Games
\tX. Exit\n''')
        
        option = input("Enter an option (A, B, C or X): ")
        option = option.upper() #making the option upper case in case the user used lower
        #Checking for each option and executing the defined function
        if option == "A":
            print("Nothing here yet")
			
        elif option == "B":
            print("Nothing here yet")
			
        elif option == "C":
            classes();

        elif option == "X":
            break
        else:
            print("Please enter a valid option!")
            
#Starting main menu loop function
main()

