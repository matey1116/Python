#University ID: 201262440 filename: Marinov_Matey-CA02.py
#October 2017
#Develop and implement a Python program to validate password criteria

#Defining the password function
def numbers():
    #Input for the number
    number = int(input("Please enter a number between 1 and 27: "))
    #boolean variables to test if the number is between 1 and 27
    number_1  = (number >= 1 )
    number_27 = (number <= 27)
    #testing if the boolean variables are true
    if number_1 and number_27:
        #if the number is bigger or equal to 10, draw a pyramid with more spaces to fix the double-digit-number problem
        if number >=10:
            #Loop for each number between 1 and the input value
            for i in range(1, number+1):
                #Loop printing spaces before the numbers to make a pyramid 
                for k in range(number-i):
                    print("  ",end="")
                #Loop printing numbers with space between them
                for j in range(0, i):
                    #If the number is <=9 print 3 spaces between numbers to make a perfect pyramid between 1 and 9
                    if i <=9:
                        print(i, end="   ")
                    #If the number is >9 print 2 spaces between numbers to make a perfect pyramid between 10 and 27
                    else:
                        print(i,end="  ")
                #printing new line after each row of numbers
                print("\n")

        #If the number is smaller than 10, draw a regular pyramid with 1 spaces between numbers
        else:
            for i in range(1, number+1):
                #Loop printing spaces before the numbers to make a pyramid
                for k in range(number-i):
                    print(" ",end="")
                #Loop printing numbers with space between them
                for j in range(0, i):
                    print(i,end=" ")
                #printing new line after each row of numbers
                print("\n")
    #Exception if number is < 1
    elif not number_1:
        print("Your number is smaller than 1")
    #Exception if number > 27
    else:
        print("Your number is larger than 27")
    
        
#Defining the main menu function
def main():
    while True:
        print('''\n\tMain Menu
\tA. Numbers
\tB. Strings
\tC. Games
\tX. Exit\n''')
        
        option = input("Enter an option (A, B, C or X): ")
        option = option.upper() #making the option upper case in case the user used lower
        #Checking for each option and executing the defined function
        if option == "A":
            numbers()
			
        elif option == "B":
            print("Nothing here yet")
			
        elif option == "C":
            print("Nothing here yet")

        elif option == "X":
            break
            
#Starting main menu loop function
main()


