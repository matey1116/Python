#University ID: 201262440, Marinov_Matey-CA06.py
#November, 2017
#Translatinga  morse code message with dictionaries, reversing, sorting dictionaries

#Function to reverse the comple sorted morse-code book
def dictionary_reverse(complete_morse_code_sorted):

    #Reversing complete morse code dictionary to translate the morse code message
    complete_morse_code_reverse = {value_reverse: key_reverse for key_reverse, value_reverse in complete_morse_code_sorted.items()}

    #Adding "/" to dictionary to print as a space
    complete_morse_code_reverse["/"] = " "

    return complete_morse_code_reverse

#Function for sorting the extended morse-code book with vowels
def dictionary_sort(complete_morse_code):

    #Sorting the complete morse code dictionary
    complete_morse_code = dict(sorted(complete_morse_code.items()))

    return complete_morse_code
#Function for extended requirement to create a dictionary from 2 lists and print it in revese order
def numbers():
    #Creating 2 lists with numbers and morse code.
    number_list = (0,1,2,3,4,5,6,7,8,9)
    number_morse_list = ("----", ".----", "..---", "...--", "....-", ".....", "-....", "--...", "---..", "----.")

    #combining the 2 lists to create a dictioanry
    number_list_together = list(zip(number_list, number_morse_list))
    number_dictionary = dict(number_list_together)

    #Reversing the order of the dictionary
    number_dictionary_reverse_order = dict(sorted(number_dictionary.items(), reverse=True))

    print()
    print("Reverse order of the morse code dictionary for numbers:")
    print(number_dictionary_reverse_order)

def print_translated_message(complete_morse_code_sorted, morse_code_no_vowels, translated_message):

    #Printing the original morse code, extended morse code and translated message
    print("The translated message says: ",end="")
    for key, value in translated_message.items():
        print(value,end="")
    print()
    print()
    print("Original Morse-code book:")
    print(morse_code_no_vowels)
    print()
    print("Extended Morse-code book with vowels in alphabetical order:")
    print(complete_morse_code_sorted)

def translated_message(complete_morse_code_reversed, original_message, complete_morse_code, morse_code_no_vowels, complete_morse_code_sorted):

    #Translating the morse code message with the reversed complete dictionary
    translated_message = {key_translate: complete_morse_code_reversed.get(value_translate, value_translate) for key_translate, value_translate in original_message.items()}

    print_translated_message(complete_morse_code_sorted, morse_code_no_vowels, translated_message)

#Function to print original morse code message
def original_message_print(original_message):

    print("You have received a new message from your researcher in Alaska.")
    print("The message is in morse code and has no vowels.")
    print("The message is: ",end="")
    for value in original_message.values():
        print(value,end=" ")
    print()
    print()

#Function for combining the dictionary with no vowels and the one with vowels
def dictionary_add_sort_reverse(morse_code_no_vowels, morse_code_vowels):

    #Combining the morse-code book without vowels with the extra morse-code book to add vowels
    complete_morse_code = {**morse_code_no_vowels, **morse_code_vowels}

    #Executing the function to sort the complete dictionary
    complete_morse_code_sorted = dictionary_sort(complete_morse_code)

    #Executing the function to reverse the morse-code dictionary
    complete_morse_code_reversed = dictionary_reverse(complete_morse_code_sorted)

    return complete_morse_code, complete_morse_code_reversed, complete_morse_code_sorted

def string():
    #Variable initialisation
    original_message = {1:'.--', 2:'....', 3:'-.--', 4:'/', 5:'.-..', 6:'-.--', 7:'-.', 8:'-..-', 9:'/', 10:'-.-.', 11:'.-.', 12:'-.--'}
    morse_code_no_vowels = {'B': '-...', 'C': '-.-.', 'G': '--.', 'H': '....', 'D': '-..', 'F': '..-.', 'M': '--', 'N': '-.','J': '.---', 'K': '-.-', 'L': '.-..', 'P': '.--.', 'Q': '--.-', 'R': '.-.', 'V': '...-','W': '.--', 'X': '-..-', 'S': '...', 'T': '-', 'Y': '-.--', 'Z': '--..'}
    morse_code_vowels = {'A': '.-', 'E': '.', 'I': '..', 'O': '---', 'U': '..-'}

    #Executing the function to add vowels, sort and reverse
    complete_morse_code, complete_morse_code_reversed, complete_morse_code_sorted = dictionary_add_sort_reverse(morse_code_no_vowels, morse_code_vowels)


    #print(complete_morse_code)
    original_message_print(original_message)

    #Starting Function
    translated_message(complete_morse_code_reversed, original_message, complete_morse_code, morse_code_no_vowels, complete_morse_code_sorted)

    #Starting numbers function
    numbers()

def main():
    while True:
        print('''\n\tMain Menu
\tA. Numbers
\tB. Strings
\tC. Games
\tX. Exit\n''')

        option = input("Enter an option (A, B, C or X): ")
        option = option.upper() #making the option upper case in case the user used lower
        #Checking for each option and executing the defined function
        if option == "A":
            print("Nothing here yet")

        elif option == "B":
            string()

        elif option == "C":
            print("Nothing here yet")

        elif option == "X":
            break
        else:
            print("Please enter a valid option!")

#Starting main menu loop function
main()
