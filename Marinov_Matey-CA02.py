#University ID: 201262440 filename: Marinov_Matey-CA02.py
#October 2017
#Develop and implement a Python program to validate password criteria

#Defining the password function
def password(): 
    password = input("Please enter a password between 8 and 15 characters: ")
    if len(password) >= 8 and len(password)<= 15:

        #Using boolean variables to test if the password contains only numbers or only letters
        numeric = password.isnumeric()
        alphabetic = password.isalpha()

        #Testing if the password has both letters and numbers
        if not numeric and not alphabetic:
            print("Your password contains both numbers and letters")
        #Testing if the password has only numbers
        elif numeric:
            print("Your password contains only numbers")
        #Exception when the password has only letters
        else:
            print("Your password contains only letters")
        
    else:
        print("The password is not between 8 and 15 characters")
        
#Defining the main menu function
def main():
    while True:
        print('''\n\tMain Menu
\tA. Numbers
\tB. Strings
\tC. Games
\tX. Exit\n''')
        
        option = input("Enter an option (A, B, C or X): ")
        option = option.upper() #making the option upper case in case the user used lower
        
        if option == "B":
            password()
			
		elif option == "A":
			print("Nothing here yet")
			
		elif option == "C":
			print("Nothing here yet")

        elif option == "X":
            break

main()

# Test-1 Input = 123456789  Output = Your password contains only numbers
# Test-2 Input = asjdnaiofg Output = Your password contains only letters
# test-3 Input = 123        Output = The password is not between 8 and 15 characters
# Test-4 Input = 12345Akdd  Output = Your password contains both numbers and letters
